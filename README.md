# TGCR Debug Probe

This is a mod that adds debug probe for TGCR.

The probe shall:
* Allow to select replacement type (right click air).
* Allow to perform replacement (punch).
* Allow to select triggering node (right click node).
