
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

local mod = {
	modname = modname,
	modpath = modpath,
}
_G[modname] = mod
local tgcr_debug_probe = mod


local smartfs = dofile(mod.modpath.."/smartfs.lua")
mod.smartfs = smartfs

local function t_green(str)
	return minetest.colorize("#00ff00", str)
end
local function t_yellow(str)
	return minetest.colorize("#cc6600", str)
end

local function chat_display(name, label, value)
	minetest.chat_send_player(name, t_green(label).." "..t_yellow(value))
end

local function chat_warning(name, msg)
	minetest.chat_send_player(name, t_yellow(msg))
end

------------------------------------
-- tgcr replacement types as known
-- from nodes_nature
--
-- selection dialog box
------------------------------------

local tgcr_replacement_form =
smartfs.create("tgcr_debug_probe:select_replacement", function(state)
	state:size(10,10)
	local lb = state:listbox(0.5,0.5,9,9, "replacements")
	for _,v in pairs(nodes_nature.replacement_types) do
		lb:addItem(v)
	end
	lb:onClick(function(self, state, idx, player_name)
		local item = self:getItem(idx)
		local player = minetest.get_player_by_name(player_name)
		local itemstack = player:get_wielded_item()
		local meta = itemstack:get_meta()
		meta:set_string("tgcr_debug_probe:replacement_type", item)
		player:set_wielded_item(itemstack)
	end)
end)

function tgcr_replacement_select(user, itemstack)
	tgcr_replacement_form:show(user:get_player_name())
	return nil
end
------------------------------------
-- use the probe
------------------------------------

local function tgcr_probe_left(itemstack, user, pointed_thing)
	local meta = itemstack:get_meta()
	local replacement_type = meta:get_string("tgcr_debug_probe:replacement_type")
	if pointed_thing.type == "nothing" or replacement_type == "" then
		return tgcr_replacement_select(user, itemstack)
	elseif pointed_thing.type == "node" then
		local player_name = user:get_player_name()
		if minetest.is_protected(pointed_thing.above, player_name)
		or minetest.is_protected(pointed_thing.under, player_name) then
			return nil
		end
		local activation_source = meta:get_string("tgcr_debug_probe:activation_source")
		--chat_display(user:get_player_name(), replacement_type, activation_source)
		tgcr.make_replacement(pointed_thing.under, replacement_type, activation_source)
	else
		return nil
	end
end

------------------------------------
-- configure the probe
------------------------------------

local function tgcr_probe_config(itemstack, user, pointed_thing)
	return tgcr_replacement_select(user, itemstack)
end


local function tgcr_probe_select(itemstack, user, pointed_thing)
	if pointed_thing and pointed_thing.type == "node" then
		local node_under = minetest.get_node(pointed_thing.under)
		local meta = itemstack:get_meta()
		meta:set_string("tgcr_debug_probe:activation_source", node_under.name)
		return itemstack
	else
		return tgcr_replacement_select(user, itemstack)
	end
end


-- Group probe (only for developers)
minetest.register_craftitem("tgcr_debug_probe:probe", {
	description = "TGCR Probe",
	inventory_image = "tgcr_debug_probe_probe.png",
	wield_image = "tgcr_debug_probe_probe.png^[transformR90",
	stack_max = 1,
	-- left click
	on_use = tgcr_probe_left,
	-- right click node
	on_place = tgcr_probe_select,
	-- right click air
	on_secondary_use = tgcr_probe_config,
})
